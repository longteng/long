<?php

namespace app\api\model;

class Theme extends Base
{
    protected $table = 'theme';

    /**
     * 自动写入增加与修改日期
     * @var string
     */
    protected $autoWriteTimestamp = 'datetime';

    /**
     * 软删除字段
     * @var string
     */
    protected $deleteTime = 'delete_time';

    /**
     * 只读字段
     * @var array
     */
    protected $readonly = ['id'];
    /**
     * 关联Image
     * 要注意belongsTo和hasOne的区别
     * 带外键的表一般定义belongsTo，另外一方定义hasOne
     */
    public function topicImg()
    {
        return $this->belongsTo('Image', 'topic_img_id', 'id');
    }

    public function headImg()
    {
        return $this->belongsTo('Image', 'head_img_id', 'id');
    }

    /**
     * 关联product，多对多关系
     */
    public function products()
    {
        return $this->belongsToMany(
            'Goods', 'theme_product', 'product_id', 'theme_id');
    }

    public function getThemeById($id){
        $theme = self::with('topicImg,headImg')->find($id);
//        if ($theme){
//            $theme = $theme->hidden(['banner_item'=>['img_id','update_time']])->toArray();
//        }
        return $theme;
    }

    public function getThemeProduct($id){
        $product = self::with('products,headImg')->find($id);
        if ($product){
            $product = $product->hidden(['products'=>['pivot','update_time']])->toArray();
        }
        return $product;
    }
}