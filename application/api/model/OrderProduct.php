<?php

namespace app\api\model;

class OrderProduct extends Base
{
    protected $table = 'order_product';

    /**
     * 自动写入增加与修改日期
     * @var string
     */
    protected $autoWriteTimestamp = 'datetime';

    /**
     * 软删除字段
     * @var string
     */
    protected $deleteTime = 'delete_time';

    /**
     * 只读字段
     * @var array
     */
    protected $readonly = ['id'];

    /**
     * 允许上传的字段
     * @var array
     */
    protected $field = ['order_id','product_id','count','create_time','update_time','deleted_time'];

}