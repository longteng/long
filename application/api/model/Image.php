<?php

namespace app\api\model;

use think\Model;

class Image extends Base
{
    protected $hidden = ['delete_time', 'id'];

    public function getUrlAttr($value, $data)
    {
        return $this->prefixImgUrl($value, $data);
    }
}

