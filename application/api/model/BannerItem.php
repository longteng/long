<?php

namespace app\api\model;

class BannerItem extends Base
{
    protected $table = 'banner_item';

    /**
     * 自动写入增加与修改日期
     * @var string
     */
    protected $autoWriteTimestamp = 'datetime';

    /**
     * 软删除字段
     * @var string
     */
    protected $deleteTime = 'delete_time';

    /**
     * 只读字段
     * @var array
     */
    protected $readonly = ['id'];

    protected $hidden = ['id','img_id', 'delete_time'];

    public function banner()
    {
        return $this->belongsTo('Banner', 'type', 'id');
    }

    public function img()
    {
        return $this->belongsTo('Image', 'img_id', 'id');
    }

}