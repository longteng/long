<?php

namespace app\api\model;

class Goods extends Base
{
    protected $table = 'product';

    /**
     * 自动写入增加与修改日期
     * @var string
     */
    protected $autoWriteTimestamp = 'datetime';

    /**
     * 软删除字段
     * @var string
     */
    protected $deleteTime = 'delete_time';

    protected $hidden = [
        'delete_time', 'main_img_id', 'pivot', 'from', 'category_id',
        'create_time', 'update_time'];

    /**
     * 只读字段
     * @var array
     */
    protected $readonly = ['id'];

    /**
     * 关联商品图片表
     * @return \think\model\relation\HasMany
     */
    public function productImg()
    {
        return $this->hasMany('ProductImg', 'product_id','id');
    }

    /**
     * 关联商品图片表
     * @return \think\model\relation\HasMany
     */
    public function category()
    {
        return $this->belongsTo('category', 'category_id','id');
    }

    /**
     * 关联属性表
     * @return \think\model\relation\HasMany
     */
    public function productProperty()
    {
        return $this->hasMany('ProductProperty', 'product_id','id');
    }

    /**
     * 拼接主图路径
     * @param $value
     * @param $data
     * @return string
     */
    public function getMainImgUrlAttr($value, $data)
    {
        return $this->prefixImgUrl($value, $data);
    }


    /**
     * name查询条件
     * @param $query
     * @param $value
     * @param $data
     */
    public function searchNameAttr($query, $value, $data)
    {
        $query->where('name','like', '%'. $value . '%');
    }

    public function searchAddTimeAttr($query, $value, $data)
    {
        $query->whereBetweenTime('add_time', $value[0], $value[1]);
    }

    public function searchDepartmentIdAttr($query, $value, $data)
    {
        $query->whereBetween('department_id', $value);
    }

    public function searchTest(){
        $res =self::withSearch(['name','department_id', 'add_time'], [
            'name'			=>	'name',
            'department_id'			=>	'1,3',
            'add_time'			=>	[1,2],
        ])->select();
    }

    /**
     * 获取最新添加商品
     * @param $count
     * @return array|\PDOStatement|string|\think\Collection
     */
    public function getMostRecent($count){
        return $this->limit($count)->order('create_time desc')->select();
    }

    /**
     * 获取商品详情
     * @param $id
     * @return array|null|\PDOStatement|string|\think\Model
     */
    public function getInfoById($id){
//        $member = $this->where('nickname',$data['name'])->whereOr('mobile',$data['name'])->limit(1)->field('id,pwd')->select();
//        $member = $this->where('nickname',$data['name'])->whereOr('mobile',$data['name'])->limit(1)->column('id,pwd,nickname', 'nickname');
//        $member = $this->where('nickname',$data['name'])->whereOr('mobile',$data['name'])->limit(1)->value('pwd');

        $product = self::with(
            [
                'productImg' => function ($query)
                {
                    $query->with(['image'])
                        ->order('order', 'asc');
                },'productProperty'])->find($id);
        return $product;
    }

    /**
     * @param $categoryID
     * @param bool $paginate
     * @param int $page
     * @param int $size
     * @return array|\PDOStatement|string|\think\Collection|\think\Paginator
     */
    public function getProductsByCategoryID( $categoryID, $paginate = true, $page = 1, $size = 30){
        $product = self::with('category')->where('category_id','=', $categoryID);
        if(!$paginate){
            return $product->select();
        }
        return $product->paginate($size,false,['page'=>$page]);
    }
}