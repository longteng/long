<?php

namespace app\api\model;

class Banner extends Base
{
    protected $table = 'banner';

    /**
     * 自动写入增加与修改日期
     * @var string
     */
    protected $autoWriteTimestamp = 'datetime';

    /**
     * 软删除字段
     * @var string
     */
    protected $deleteTime = 'delete_time';

    /**
     * 只读字段
     * @var array
     */
    protected $readonly = ['id'];

    public function bannerItem()
    {
        return $this->hasMany('BannerItem', 'type', 'id');
    }

    /**
     * @param $id
     * @return array|null|\PDOStatement|string|\think\Model
     */
    public function getBannerById($id){
        $banners = self::with('bannerItem.img')->find($id);
        if ($banners){
            $banners = $banners->hidden(['banner_item'=>['img_id','update_time']])->toArray();
        }
        return $banners;
    }

}