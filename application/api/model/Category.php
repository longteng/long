<?php

namespace app\api\model;

class Category extends Base
{
    protected $table = 'category';

    /**
     * 自动写入增加与修改日期
     * @var string
     */
    protected $autoWriteTimestamp = 'datetime';

    /**
     * 软删除字段
     * @var string
     */
    protected $deleteTime = 'delete_time';

    /**
     * 只读字段
     * @var array
     */
    protected $readonly = ['id'];

    /**
     * 关联product，多对多关系
     */
    public function products()
    {
        return $this->hasMany('Goods', 'category_id', 'id');
    }

    public function image()
    {
        return $this->belongsTo('Image', 'topic_img_id', 'id');
    }

    /**
     * 获取所有分类
     * @return array|\PDOStatement|string|\think\Collection
     */
    public function getCategoryList(){
        $categories = self::with('image')->select();
        return $categories;
    }

    public function getCategories($ids)
    {
        $categories = self::with('products')
            ->with('products.img')
            ->select($ids);
        return $categories;
    }

    public function getCategory($id)
    {
        $category = self::with('products')
            ->with('products.img')
            ->find($id);
        return $category;
    }
}