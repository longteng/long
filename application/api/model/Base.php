<?php

namespace app\api\model;

use think\Model;
use think\model\concern\SoftDelete;

class Base extends Model
{
    // 软删除，设置后在查询时要特别注意whereOr
    // 使用whereOr会将设置了软删除的记录也查询出来
    // 可以对比下SQL语句，看看whereOr的SQL
    use SoftDelete;

    protected $hidden = ['delete_time'];

    protected function  prefixImgUrl($value, $data){
        $finalUrl = $value;
        if( isset($data['from']) && $data['from'] == 1){
            $finalUrl = config('app.host_img').$value;
        }
        return $finalUrl;
    }

    protected function  preImgUrl($value){
        return config('app.host_img').$value;
    }
}