<?php

namespace app\api\model;

class ThirdApp extends Base
{
    protected $table = 'third_app';

    /**
     * 自动写入增加与修改日期
     * @var string
     */
    protected $autoWriteTimestamp = 'datetime';

    /**
     * 软删除字段
     * @var string
     */
    protected $deleteTime = 'delete_time';

    /**
     * 只读字段
     * @var array
     */
    protected $readonly = ['id'];

    /**验证
     * @param $ac
     * @param $se
     * @return $this
     */
    public static function check($ac, $se){
        $res = self::where([['app_id','=',$ac],['app_secret', '=',$se]])->find();
//        $res = password_verify($se, $res['pwd']);
        return $res;
    }
}