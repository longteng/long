<?php

namespace app\api\model;

class User extends Base
{
    protected $table = 'user';

    /**
     * 自动写入增加与修改日期
     * @var string
     */
    protected $autoWriteTimestamp = 'datetime';

    /**
     * 软删除字段
     * @var string
     */
    protected $deleteTime = 'delete_time';

    /**
     * 只读字段
     * @var array
     */
    protected $readonly = ['id'];

    /**
     * 允许上传的字段
     * @var array
     */
    protected $field = ['openid','nickname','extend','create_time','update_time','deleted_time'];

    /**
     * 关联地址
     * @return \think\model\relation\HasOne
     */
    public function address()
    {
        return $this->hasOne('UserAddress', 'user_id', 'id');
    }

    /**
     * @return \think\model\relation\HasMany
     */
    public function order()
    {
        return $this->hasMany('Order', 'user_id', 'id');
    }

    /**
     * @param $openid
     * @return array|\PDOStatement|string|\think\Collection
     */
    public function getByOpenId($openid){
        return $this->where('openid',$openid)->find();
    }
    public function saveByOPenId($openid){
        $this->save(
            ['openid' =>$openid]
        );
        return $this->id;
    }
}