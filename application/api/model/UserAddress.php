<?php

namespace app\api\model;

class UserAddress extends Base
{
    protected $table = 'user_address';

    protected $hidden =['id', 'delete_time'];
    /**
     * 允许上传的字段
     * @var array
     */
//    protected $field = ['openid','nickname','extend','create_time','update_time','deleted_time'];

    /**
     * 只读字段
     * @var array
     */
    protected $readonly = ['id', 'user_id'];

    public function getByUid($uid){
        return $this->where('user_id',$uid)->find();
    }
}


