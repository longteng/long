<?php

namespace app\api\model;

class ProductProperty extends Base
{
    protected $table = 'product_property';

    /**
     * 自动写入增加与修改日期
     * @var string
     */
    protected $autoWriteTimestamp = 'datetime';

    /**
     * 软删除字段
     * @var string
     */
    protected $deleteTime = 'delete_time';

    /**
     * 只读字段
     * @var array
     */
    protected $readonly = ['delete_time'];

    public function  product(){
        return $this->belongsTo('product','product_id','id');
    }
}