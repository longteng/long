<?php

namespace app\api\model;

class ProductImg extends Base
{
    protected $table = 'product_image';

    /**
     * 自动写入增加与修改日期
     * @var string
     */
    protected $autoWriteTimestamp = 'datetime';

    /**
     * 软删除字段
     * @var string
     */
    protected $deleteTime = 'delete_time';

    /**
     * 只读字段
     * @var array
     */
    protected $readonly = ['delete_time'];

    public function  image(){
        return $this->belongsTo('Image','img_id','id');
    }
}