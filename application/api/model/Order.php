<?php

namespace app\api\model;


class Order extends Base
{
    protected $table = 'order';

    /**
     * 自动写入增加与修改日期
     * @var string
     */
    protected $autoWriteTimestamp = 'datetime';

    /**
     * 软删除字段
     * @var string
     */
    protected $deleteTime = 'delete_time';

    /**
     * 只读字段
     * @var array
     */
    protected $readonly = ['id','order_no'];

    /**
     * 允许上传的字段
     * @var array
     */
    protected $field = ['order_no','user_id','total_price','status','snap_img','snap_name','total_count',
        'snap_items','snap_address','prepay_id','create_time','update_time','deleted_time'];


    public function getSnapItemsAttr($value, $data)
    {
        $finalUrl = json_decode($value);
        foreach ($finalUrl as $k=>$v){
            $finalUrl[$k]->main_img_url = config('app.host_img').$v->main_img_url;
        }
        return $finalUrl;
    }
    public function getSnapAddressAttr($value)
    {
        $finalUrl = json_decode($value);
        return $finalUrl;
    }
    public function getSnapImgAttr($value)
    {
       return $this->preImgUrl($value);
    }

    /**
     * 获取商品详情
     * @param $id
     * @return array|null|\PDOStatement|string|\think\Model
     */
    public function getInfoById($id){

        $product = $this->find($id);
        $product->hidden(['prepay_id']);
        return $product;
    }

    /**
     * @param $uid
     * @param $page
     * @param $size
     * @return array
     */
    public function getOrdersByUser($uid, $page, $size){
        $pagingData = self::where('user_id', '=', $uid)
            ->order(['id'=>'desc','create_time'=>'desc'])
            ->paginate($size, true, ['page' => $page]);
        if($pagingData->isEmpty()){
            return [
                'current_page' => $pagingData,
                'data' => []
            ];
        }
        $data = $pagingData->hidden(['snap_address'])->toArray();
        return [
            'current_page' => $pagingData->currentPage(),
            'data' => $data['data']
        ];
    }

    /**
     * @param int $page
     * @param int $size
     * @return array
     */
    public static function getSummaryByPage($page=1, $size=20){
        $pagingData = self::order(['id'=>'desc','create_time'=>'desc'])
            ->paginate($size, true, ['page' => $page]);
        if($pagingData->isEmpty()){
            return [
                'current_page' => $pagingData,
                'data' => []
            ];
        }
        $data = $pagingData->hidden(['snap_address'])->toArray();
        return [
            'current_page' => $pagingData->currentPage(),
            'data' => $data['data']
        ];
    }

}