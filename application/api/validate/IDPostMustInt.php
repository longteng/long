<?php
namespace app\api\validate;


class IDPostMustInt extends BaseValidate
{
    protected $rule = [
        'id' => 'require|isPositiveInteger',
    ];
}