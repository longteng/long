<?php
namespace app\api\validate;

use app\lib\exception\ParameterException;

class OrderValidate extends BaseValidate
{
    protected $rule = [
        'products' => 'require|checkProducts',
    ];

    protected $singleRule = [
        'product_id' => 'require|isPositiveInteger',
        'count' => 'require|isPositiveInteger',
    ];
    protected $message = [
        'products.require'  =>  '请上传商品',
    ];

    protected $scene = [
        'add'   =>  ['products'],
        'edit'  =>  ['products'],
    ];

    protected function checkProducts($values){
        if (empty($values)){
            throw new ParameterException([
                'msg' => '商品列表不能为空',
                'code' => 400,
            ]);
        }
        foreach ($values as $value){
            $this->checkProduct($value);
        }
        return true;
    }

    protected function checkProduct($value){

        $validate = new BaseValidate($this->singleRule);
        $result = $validate->check($value);
        if (!$result){
            throw new ParameterException([
                'msg' => $validate->getError(),
                'code' => 400,
            ]);
        }
    }
}