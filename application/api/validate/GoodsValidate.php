<?php
namespace app\api\validate;

class GoodsValidate extends BaseValidate
{
    protected $rule = [
        'name'  =>  'require|max:25',
        'category_id'    =>  'require|number',
//        'intro'    =>  'require', // 商品描述
        'price'=>'require', // 商品价格
        'marque'=>'require|unique:goods', // 商品价格
        'market_price'=>'require', // 商品市场价格
        'stock'=>'require', // 库存
//        'picture_url'=>'require', // 封面图片

    ];

    protected $message = [
        'name.require'  =>  '请填写商品名称',
        'category_id.require'    =>  '请选择商品分类',
//        'intro.require'    =>  '请填写商品描述', // 商品描述
        'price.require'=>'请填写商品价格', // 商品价格
        'marque.require'=>'请填写商品编号', // 商品价格
        'marque.unique'=>'商品编号已存在', // 商品价格
        'market_price.require'=>'请填写商品市场价格', // 商品市场价格
        'stock.require'=>'请填写库存', // 库存
//        'picture_url.require'=>'请上传商品封面图', // 封面图片
    ];

    protected $scene = [
        'add'   =>  ['name','category_id','price','marque','market_price','stock'],
        'edit'  =>  ['name','category_id','price','market_price','stock'],
    ];
}