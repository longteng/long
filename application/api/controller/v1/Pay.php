<?php
namespace app\api\controller\v1;

use app\api\controller\Base;
use app\api\service\PayService;
use app\api\service\WxNotify;
use app\api\validate\IDPostMustInt;
use app\api\service\Order as OrderService;

class Pay extends Base
{
    protected $beforeActionList = [
        'checkExclusivePrimaryScope' => ['only' => 'payOrder']
    ];

    /**
     * 订单支付
     * @return array
     */
    public function payOrder(){
        return 1;
        (new IDPostMustInt())->goCheck();
        $id = input('post.id');
        $pay = new PayService($id);
        return $pay->pay();
    }

    /**
     * @return array
     */
    public function payNotify(){
        $pay = new WxNotify();
        $pay->payCallback();
    }
}
