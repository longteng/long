<?php
namespace app\api\controller\v1;

use app\api\validate\IDPostMustInt;

use think\facade\Cache;
use think\facade\Log;
class Banner extends Auth
{
    protected $beforeActionList = [
//        'checkPrimaryScope' => ['only' => 'bannerList']
    ];

    /**
     * 通过id获取对应活动详情
     * @param int $id
     * @return \think\response\Json
     */
    public function bannerList($id=1)
    {
//        cache('file',2212, 100);

//        Cache::store('redis')->set('file','value', 300);
//        return Cache::store('redis')->get('file');
        $banner = input('id');
        if ($banner) $id = $banner;
        $list = $this->model->getBannerById($id);
        Log::sql($list);
        return json($list, 200);
    }
    /**
     * @return \think\response\Json
     */
    public function bannerInfo()
    {
        (new IDPostMustInt())->goCheck();
        return json($this->model->find(input('id')),200);
    }
}
