<?php
namespace app\api\controller\v1;

use app\api\controller\Base;
use app\api\validate\GoodsValidate;
use app\api\validate\IDPostMustInt;
use app\api\validate\CountValidate;
use app\api\validate\PagingParameter;

class Goods extends Base
{
    protected $beforeActionList = [
        'checkPrimaryScope' => ['only' => 'goodsInfo']
    ];

    /**
     * @return \think\response\Json
     */
    public function goodsList()
    {
          $arr = $this->model->limit(3)->select();
          return json($arr, 200);
    }
    /**
     * @return \think\response\Json
     */
    public function goodsInfo()
    {
        (new IDPostMustInt())->goCheck();
        $goodsInfo = $this->model->getInfoById(input('id'));
        return json($goodsInfo,200);
    }

    /**
     * @param int $id
     * @param int $page
     * @param int $size
     * @return array
     */
    public function goodsCategory($id=0, $page=1, $size=10){
        (new IDPostMustInt())->goCheck();
        (new PagingParameter())->goCheck();
        $pagProducts = $this->model->getProductsByCategoryID($id, true, $page, $size);
        if ($pagProducts->isEmpty())
        {
            // 对于分页最好不要抛出MissException，客户端并不好处理
            return [
                'current_page' => $pagProducts->currentPage(),
                'data' => []
            ];
        }
        return $pagProducts;
    }

    /**
     * @return int
     */
    public function goodsAdd()
    {
        (new GoodsValidate())->goCheck();
        return 1;
    }
    /**
     * 获取指定数量的最近商品
     * @url /product/recent?count=:count
     * @param int $count
     * @return mixed
     * @throws ParameterException
     */
    public function getRecent($count = 8)
    {
        (new CountValidate())->goCheck();
        $get = input('count');
        if ($get) $count = $get;
        $products = $this->model->getMostRecent($count);
        return $products;
    }
}
