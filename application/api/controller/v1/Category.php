<?php
namespace app\api\controller\v1;

use app\api\controller\Base;
use app\lib\exception\MissException;

class Category extends Base
{
    protected $beforeActionList = [
//        'checkPrimaryScope' => ['only' => 'categoryList']
    ];

    /**
     * @return \think\response\Json
     * @throws MissException
     */
    public function categoryList()
    {
        $category = $this->model->getCategoryList();
        if (empty($category)){
            throw new MissException([
                'msg' => '还没有任何类目',
                'errorCode' => 50000
            ]);
        }
        return json($category, 200);
    }
}
