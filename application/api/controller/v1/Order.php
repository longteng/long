<?php
namespace app\api\controller\v1;

use app\api\controller\Base;
use app\api\validate\IDPostMustInt;
use app\api\validate\OrderValidate;
use app\api\service\Token;
use app\api\service\Order as OrderService;
use app\api\model\Order as OrderModel;
use app\api\validate\PagingParameter;
use app\lib\exception\SuccessMessage;

class Order extends Base
{
    protected $beforeActionList = [
        'checkPrimaryScope' => ['only' => 'orderPlace'],
    ];
    /**
     * 下单
     * @return array
     */
    public function orderPlace()
    {
        (new OrderValidate())->goCheck();
        $products = input('post.products');
        $uid = Token::getCurrentUid();
        $orderService = new OrderService();
        $status = $orderService->place($uid, $products);
        return $status;
    }

    /**
     * 获取订单详情
     * @return \think\response\Json
     */
    public function orderInfo(){
        (new IDPostMustInt())->goCheck();
        $goodsInfo = $this->model->getInfoById(input('id'));
        return json($goodsInfo,200);
    }

    /**
     * @param int $page
     * @param int $size
     * @return array
     */
    public function orderListUser($page = 1, $size = 15)
    {
        (new PagingParameter())->goCheck();
        $uid = Token::getCurrentUid();
        $pagingOrders = $this->model->getOrdersByUser($uid, $page, $size);
        return $pagingOrders;
    }

    /**
     * 获取全部订单简要信息（分页）
     * @param int $page
     * @param int $size
     * @return array
     * @throws \app\lib\exception\ParameterException
     */
    public function getSummary($page=1, $size = 20){
        (new PagingParameter())->goCheck();
        $pagingOrders = $this->model->getSummaryByPage($page, $size);
        return $pagingOrders;
    }

    /**
     * @param $id
     * @return SuccessMessage
     */
    public function delivery($id){
        (new IDPostMustInt())->goCheck();
        $order = new OrderService();
        $success = $order->delivery($id);
        if($success){
            return new SuccessMessage();
        }
    }
}
