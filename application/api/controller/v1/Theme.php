<?php
namespace app\api\controller\v1;

use app\api\validate\GoodsValidate;
use app\api\validate\IDPostMustInt;

class Theme extends Auth
{
    protected $beforeActionList = [
//        'checkPrimaryScope' => ['only' => 'themeList']
    ];

    /**
     * @return \think\response\Json
     */
    public function themeList()
    {
        $list = $this->model->with('topicImg,headImg')->select();
        return json($list, 200);
    }
    /**
     * @return \think\response\Json
     */
    public function themeInfo()
    {
        (new IDPostMustInt())->goCheck();
        $themeInfo = $this->model->getThemeById(input('id'));
        return json($themeInfo,200);
    }
    /**
     * @return \think\response\Json
     */
    public function themeProduct()
    {
        (new IDPostMustInt())->goCheck();
        $products = $this->model->getThemeProduct(input('id'));
        return json($products,200);
    }
    public function themeAdd()
    {
//       $param = $this->request->param();
        (new GoodsValidate())->goCheck();
        return 1;
    }
}
