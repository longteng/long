<?php
namespace app\api\controller\v1;

use app\api\service\AccessToken;
use app\api\service\UserToken;
use app\api\service\Token as TokenService;
use app\api\service\ThirdToken;
use app\api\service\WxGzhToken;
use app\api\service\WxCallbackTest;
use app\api\model\User as UserModel;
use app\api\validate\AppTokenGet;
use app\lib\exception\ParameterException;

class Token
{
    private $userToken;
    private $wxGzhToken;


    public function __construct(WxGzhToken $wxGzhToken)
    {
        $this->wxGzhToken = $wxGzhToken;
    }

    /**
     * 根据微信小程序code，返回token
     * @param string $code
     * @return string
     */
    public function getToken($code='')
    {
        $this->userToken = new UserToken($code);
        $token = $this->userToken->get($code);
        $arrToken = array(
            'token' => $token
        );
        return json($arrToken, 200);
    }
    /**
     * 验证token
     * @param string $token
     * @return array
     * @throws ParameterException
     */
    public function verifyToken($token='')
    {
        if(!$token){
            throw new ParameterException([
               'msg' => 'token不允许为空',
            ]);
        }
        $valid = TokenService::verifyToken($token);
        return [
            'isValid' => $valid
        ];
    }

    /**
     * @param string $ac
     * @param string $se
     * @return array
     * @throws ParameterException
     * @throws \app\lib\exception\TokenException
     */
    public function getAppToken($ac='', $se=''){
        (new AppTokenGet())->goCheck();
        $thirdToken = new ThirdToken();
        $token = $thirdToken->get($ac, $se);
        return [
          'token' => $token
        ];
    }

    /**
     * 获取openid
     * @return \think\response\Json
     */
    public function getOpenid(){
        $userModel = new UserModel();
        $res = $userModel->save(
            [
                'openid' => 'long'
            ]
        );
        return $res;
    }

    /**
     * 微信公众号创建菜单
     * @return \think\response\Json
     */
    public function createMenu(){
        $menu='{
         "button":[
         {	
              "type":"click",
              "name":"新闻",
              "key":"news"
          },
          {
               "name":"游戏",
               "sub_button":[
               {	
                   "type":"view",
                   "name":"飞行学校",
                   "url":"http://47.75.205.126/api/v1/wx/get_openid"
                },
                {
                   "type":"view",
                   "name":"丹迪洞穴探险",
                   "url": "http://47.75.205.126/api/v1/wx/add_user"
                }]
           }]
        }';
        $res = $this->wxGzhToken->createMenu($menu);
        return json($res, 200);
    }

    /**
     * 公众号配置接口配置信息URL
     */
    public function wxTest(){
        $echoStr = input('get.echostr');
        $wxTest = new WxCallbackTest();
        $res = $wxTest->valid($echoStr);
        echo $res;
        exit;
    }
}
