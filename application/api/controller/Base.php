<?php
namespace app\api\controller;

use think\Controller;
use app\api\service\Token;
class Base extends Controller
{
    public $model;

    /**
     * Base初始化
     */
    public function initialize()
    {
        $this->initModel();

        // 二级控制器初始化
        if (method_exists($this, '_init')) {
            $this->_init();
        }

        // 三级控制器初始化
        if (method_exists($this, '_exec')) {
            $this->_exec();
        }
    }

    /**
     * 初始化当前控制器模型
     */
    public function initModel(){

        $str = $this->request->controller();
        $index_url = config('api');
        $string = $index_url.substr($str, strrpos($str,".")+1);
        if (class_exists($string)){
            $this->model = model($string);
        }else{
            $this->model = null;
        }
    }

    /**
     * 验证普通用户以上
     */
    protected function checkPrimaryScope()
    {
        Token::needPrimaryScope();
    }

    /**
     * 验证为super用户以上
     */
    protected function checkSuperScope()
    {
        Token::needSuperScope();
    }

    /**
     * 验证只能为普通用户
     */
    protected function checkExclusivePrimaryScope()
    {
        Token::needExclusivePrimaryScope();
    }
}
