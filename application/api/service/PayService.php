<?php
namespace app\api\service;

use app\lib\enum\OrderStatusEnum;
use app\lib\exception\OrderException;
use app\lib\exception\TokenException;
use app\lib\exception\WeChatException;
use think\Exception;
use app\api\model\Order as OrderModel;
use app\api\service\Order as OrderService;
use think\facade\Env;
use think\facade\Log;

require_once Env::get('root_path') . 'extend/wxPay/WxPay.Api.php';
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/8
 * Time: 10:27
 */

class PayService
{
    private $orderNo;
    private $orderId;
    private $orderModel;

    public function __construct($orderId)
    {
        if (!$orderId)
        {
            throw new Exception('订单号不允许为NULL');
        }
        $this->orderId = $orderId;
        $this->orderModel = new OrderModel();
    }
    public function pay(){
        $this->checkOrderValid();
        $orderService = new OrderService();
        $status = $orderService->checkOrderStock($this->orderId);
        if (!$status['pass']){
            return $status;
        }
        return $this->makeWxPreOrder($status['orderPrice']);
    }

    /**
     * 核对订单操作权限
     * @return bool
     * @throws OrderException
     * @throws TokenException
     */
    public function checkOrderValid(){
        $order = $this->orderModel->where('id', $this->orderId)->find();
        if (!$order){
            throw new OrderException([
                'msg' => '此订单不存在',
            ]);
        }

        if (!Token::isValidOperate($order->user_id)){
            throw new TokenException(
                [
                    'msg' => '用户与订单不匹配',
                    'errorCode' => 10003
                ]);
        }
        if($order->status != OrderStatusEnum::UNPAID){
            throw new OrderException([
                'msg' => '订单已支付过啦',
                'errorCode' => 8003,
                'code' => 400
            ]);
        }
        $this->orderNo = $order->order_no;
        return true;
    }

     private function makeWxPreOrder($totalPrice){
        $openid = Token::getCurrentTokenVar('openid');
        if (!$openid){
            throw new TokenException();
        }

        $wxPayData = new \WxPayUnifiedOrder();
        $wxPayData->SetOut_trade_no($this->orderNo);
        $wxPayData->SetTrade_type('JSAPI');
        $wxPayData->SetTotal_fee($totalPrice*100);
        $wxPayData->SetBody('零食');
        $wxPayData->SetOpenid($openid);
        $wxPayData->SetNotify_url(config('app.wx.pay_back_url'));

        return $this->getPaySignature($wxPayData);
     }

     private function getPaySignature($wxPayData){
         $config = new \WxPayConfig();
         $wxOrder = \WxPayApi::unifiedOrder($config, $wxPayData);
         //返回结果中包含 prepay_id ，此ID作为用户拉起支付时凭证，
         //同时此ID作为将来服务器向客户端推送消息的标识，因此需要保存在数据库订单表中
         // 失败时不会返回result_code
         if($wxOrder['return_code'] != 'SUCCESS' || $wxOrder['result_code'] !='SUCCESS'){
             Log::record($wxOrder,'error');
             Log::record('获取预支付订单失败','error');
             throw new WeChatException([
                 'msg' => $wxOrder['return_msg'],
             ]);
         }
         $this->recordPreOrder($wxOrder);
         $signature = $this->sign($wxOrder);
         return $signature;
     }
    private function recordPreOrder($wxOrder){
        // 将 prepay_id 保存在数据库中
        OrderModel::where('id', '=', $this->orderId)
            ->update(['prepay_id' => $wxOrder['prepay_id']]);
    }

    /**
     * @param $wxOrder
     * @return array
     */
    private function sign($wxOrder)
    {
        //调用SDK 生成签名
        $jsApiPayData = new \WxPayJsApiPay();
        //Appid
        $jsApiPayData->SetAppid(config('app.wx.app_id'));
        //timeStamp
        $jsApiPayData->SetTimeStamp((string)time());
        //nonceStr
        $rand = md5(time() . mt_rand(0, 1000));
        $jsApiPayData->SetNonceStr($rand);
        //package
        $jsApiPayData->SetPackage('prepay_id=' . $wxOrder['prepay_id']);
        //signType
        $jsApiPayData->SetSignType('md5');
        //生成签名
        $config = new \WxPayConfig();
        $jsApiPayData->SetPaySign($jsApiPayData->MakeSign($config));
        $sign = $jsApiPayData->GetPaySign();
        //获取数组
        $rawValues = $jsApiPayData->GetValues();
        $rawValues['paySign'] = $sign;
        // 删除返回的appId,防止返回给客户端
        unset($rawValues['appId']);
        return $rawValues;
    }

}