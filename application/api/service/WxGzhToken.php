<?php
namespace app\api\service;

use think\Exception;
use app\lib\exception\WeChatException;
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/8
 * Time: 10:27
 */

class WxGzhToken extends Token
{
    protected $appId;
    private $appSecret;
    protected $accessToken;
    protected $redirectUri;

    public function __construct(){
        $this->appId = config('app.wx_gzh.app_id');
        $this->appSecret = config('app.wx_gzh.app_secret');
        $this->redirectUri = config('app.wx_gzh.redirect_uri');
    }

    public function getAccessToken(){
        $res = curl_post(sprintf(
            config('app.wx_gzh.access_token'), $this->appId, $this->appSecret));
        // 注意json_decode的第一个参数true
        // 这将使字符串被转化为数组而非对象
        $wxRes = json_decode($res, true);
        if (empty($wxRes)){
            // 为什么以empty判断是否错误，这是根据微信返回
            // 规则摸索出来的
            // 这种情况通常是由于传入不合法的code
            throw new Exception('获取accessToken时异常，微信内部错误');
        }else{
            // 建议用明确的变量来表示是否成功
            // 微信服务器并不会将错误标记为400，无论成功还是失败都标记成200
            // 这样非常不好判断，只能使用errcode是否存在来判断
            if (array_key_exists('errcode', $wxRes)){
                throw new WeChatException(
                    [
                        'msg' => $wxRes['errmsg'],
                        'errorCode' => $wxRes['errcode']
                    ]);
            }else{
                $this->accessToken = $wxRes['access_token'];
                return $wxRes['access_token'];
            }

        }
    }

    /**
     * 获取用户openid
     * @return mixed
     * @throws Exception
     * @throws WeChatException
     */
    public function getOpenid(){
        $url = sprintf(
            config('app.wx_gzh.openid'), $this->appId, $this->redirectUri);
        $res = curl_post($url);
        // 注意json_decode的第一个参数true
        // 这将使字符串被转化为数组而非对象
        $wxRes = json_decode($res, true);
        if (empty($wxRes)){
            // 为什么以empty判断是否错误，这是根据微信返回
            // 规则摸索出来的
            // 这种情况通常是由于传入不合法的code
            throw new Exception('获取openid时异常，微信内部错误');
        }else{
            // 建议用明确的变量来表示是否成功
            // 微信服务器并不会将错误标记为400，无论成功还是失败都标记成200
            // 这样非常不好判断，只能使用errcode是否存在来判断
            if (array_key_exists('errcode', $wxRes)){
                throw new WeChatException(
                    [
                        'msg' => $wxRes['errmsg'],
                        'errorCode' => $wxRes['errcode']
                    ]);
            }else{
                return $wxRes;
            }

        }
    }

    /**
     * 创建菜单
     * @param $menu
     * @return mixed
     */
    public function createMenu($menu){
        $url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=".$this->getAccessToken();
        $result = curl_request($url, true, "post", $menu);
        $result = json_decode($result);
        return $result;
    }


}