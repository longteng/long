<?php
namespace app\api\service;

use app\api\model\User;
use app\lib\enum\ScopeEnum;
use app\lib\exception\CacheException;
use think\Exception;
use app\lib\exception\WeChatException;
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/8
 * Time: 10:27
 */

class UserToken extends Token
{
    protected $code;
    protected $wxLoginUrl;
    protected $wxAppID;
    protected $wxAppSecret;

    public function __construct($code)
    {
        $this->code = $code;
        $this->wxAppID = config('app.wx.app_id');
        $this->wxAppSecret = config('app.wx.app_secret');
        $this->wxLoginUrl = sprintf(
            config('app.wx.login_url'), $this->wxAppID, $this->wxAppSecret, $this->code);
    }
    public function get(){
        $res = curl_post($this->wxLoginUrl);
        // 注意json_decode的第一个参数true
        // 这将使字符串被转化为数组而非对象
        $wxRes = json_decode($res, true);
        if (empty($wxRes)){
            // 为什么以empty判断是否错误，这是根据微信返回
            // 规则摸索出来的
            // 这种情况通常是由于传入不合法的code
            throw new Exception('获取session_key及openID时异常，微信内部错误');
        }else{
            // 建议用明确的变量来表示是否成功
            // 微信服务器并不会将错误标记为400，无论成功还是失败都标记成200
            // 这样非常不好判断，只能使用errcode是否存在来判断
            if (array_key_exists('errcode', $wxRes)){
                throw new WeChatException(
                    [
                        'msg' => $wxRes['errmsg'],
                        'errorCode' => $wxRes['errcode']
                    ]);
            }else{
                return $this->grantToken($wxRes);
            }

        }
    }

    /**
     * @param $wxRes
     * @return string
     */
    // 颁发令牌
    // 只要调用登陆就颁发新令牌
    // 但旧的令牌依然可以使用
    // 所以通常令牌的有效时间比较短
    // 目前微信的express_in时间是7200秒
    // 在不设置刷新令牌（refresh_token）的情况下
    // 只能延迟自有token的过期时间超过7200秒（目前还无法确定，在express_in时间到期后
    // 还能否进行微信支付
    // 没有刷新令牌会有一个问题，就是用户的操作有可能会被突然中断
    public function grantToken($wxRes){
        $openid = $wxRes['openid'];
        $userModel = new User();
        $user =$userModel->getByOpenId($openid);
        if (!$user){
            $uid =  $userModel->saveByOpenId($openid);
        }else{
            $uid = $user->id;
        }
        $cacheValue = $this->preparaCacheValue($wxRes, $uid);
        $token = $this->saveToCache($cacheValue);
        return $token;
    }

    public function preparaCacheValue($wxRes, $uid){
        $cacheValue = $wxRes;
        $cacheValue['uid'] = $uid;
        $cacheValue['scope'] = ScopeEnum::USER;
        return $cacheValue;
    }

    public function saveToCache($cacheValue){
        $key = self::generateToken();
        $value = json_encode($cacheValue);
        $expire_in = config('app.wx.token_expire_in');

        $res = cache($key, $value, $expire_in);
        if (!$res){
            throw new CacheException();
        }
        return $key;
    }
}