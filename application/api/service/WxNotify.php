<?php
namespace app\api\service;

use app\api\model\Goods;
use app\lib\enum\OrderStatusEnum;
use think\Db;
use think\Exception;
use think\facade\Log;
use app\api\model\Order as OrderModel;
use app\api\service\Order as OrderService;
use app\api\model\Goods as GoodsModel;
use think\facade\Env;

require_once Env::get('root_path') . 'extend/wxPay/WxPay.Api.php';

class WxNotify extends \WxPayNotify
{
    //重写回调处理函数
    /**
     * @param WxPayNotifyResults $data 回调解释出的参数
     * @param WxPayConfigInterface $config
     * @param string $msg 如果回调处理失败，可以将错误信息输出到该方法
     * @return true回调出来完成不需要继续回调，false回调处理未完成需要继续回调
     */
    public function NotifyProcess($objData, $config, &$msg)
    {
        $data = $objData->GetValues();
        //TODO 1、进行参数校验
        if ($data['result_code'] == 'SUCCESS' && array_key_exists("transaction_id", $data)) {
            //TODO 2、进行签名验证
            try {
                $checkResult = $objData->CheckSign($config);
                if ($checkResult == false) {
                    //签名错误
                    Log::ERROR("签名错误...");
                    return false;
                }
            } catch (Exception $e) {
                Log::ERROR(json_encode($e));
                throw $e;
            }

            $orderNo = $data['out_trade_no'];
            Db::startTrans();
            try {
                $order = OrderModel::where('order_no', $orderNo)->lock()->find();
                if ($order->status == 1) {
                    $service = new OrderService();
                    $status = $service->checkOrderStock($order->id);
                    if ($status['pass']) {
                        $this->updateOrderStatus($order->id, true);
                        $this->reduceStock($status);
                    } else {
                        $this->updateOrderStatus($order->id, false);
                    }
                }

            } catch (Exception $ex) {
                Db::rollback();
                Log::error($ex);
                // 如果出现异常，向微信返回false，请求重新发送通知
                return false;
            }
        }

        //查询订单，判断订单真实性
        if (!$this->Queryorder($data["transaction_id"])) {
            $msg = "订单查询失败";
            return false;
        }
        return true;
    }

    /**
     * 减少库存
     * @param $status
     */
    private function reduceStock($status)
    {
        foreach ($status['pStatusArray'] as $singlePStatus) {
            GoodsModel::where('id', $singlePStatus['id'])->setDec('stock', $singlePStatus['count']);
        }
    }


    private function updateOrderStatus($orderID, $success)
    {
        $status = $success ? OrderStatusEnum::PAID : OrderStatusEnum::PAID_BUT_OUT_OF;
        GoodsModel::where('id', $orderID)->update(['status' => $status]);
    }

    /**
     * 支付回调接口
     */
    public function payCallback()
    {
        $config = new \WxPayConfig();
        $this->Handle($config, false);

    }
}
