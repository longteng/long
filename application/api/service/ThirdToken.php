<?php
namespace app\api\service;
use app\api\model\ThirdApp;
use app\lib\exception\TokenException;

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/8
 * Time: 10:27
 */

class ThirdToken extends Token
{
    public function get($ac, $se){
        $app = ThirdApp::check($ac,$se);
        if(!$app){
            throw new TokenException([
                'msg' => '授权失败',
                'errorCode' => 10004
            ]);
        }else{
            $scope = $app->scope;
            $uid = $app->id;
            $values = [
                'scope' => $scope,
                'uid' => $uid
            ];
            $token = $this->saveToCache($values);
            return $token;
        }
    }
    public function saveToCache($values){
        $token = self::generateToken();
        $expire_in = config('app.third.app_id');
        $res = cache($token, $values, $expire_in);
        if (!$res){
            throw new TokenException([
                'msg' => '服务器缓存异常',
                'errorCode' => 10005
            ]);
        }
        return $token;
    }

}