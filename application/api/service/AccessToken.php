<?php
namespace app\api\service;
use think\Exception;


/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/8
 * Time: 10:27
 */

class AccessToken
{
    private $tokenUrl;
    const TOKEN_CACHED_KEY = 'access';
    const TOKEN_EXPIRE_IN = 7000;

    public function __construct()
    {
        $url = config('app.wx.access_token');
        $url = sprintf($url, config('app.wx.app_id'), config('app.wx.app_secret'));
        $this->tokenUrl = $url;
    }

    public function get(){
        $token = $this->getFromCache();
        if(!$token){
            return $this->getFromWxServer();
        }
        else{
            return $token;
        }
    }

    private function getFromCache(){
        $token = cache(self::TOKEN_CACHED_KEY);
        if($token){
            return $token['access_token'];
        }
        return null;
    }
    private function getFromWxServer(){
        $token = curl_get($this->tokenUrl);
        $token = json_decode($token, true);
        if (!$token)
        {
            throw new Exception('获取AccessToken异常');
        }
        if(!empty($token['errcode'])){
            throw new Exception($token['errmsg']);
        }
        $this->saveToCache($token);
        return $token;
    }

    private function saveToCache($value){
        cache(self::TOKEN_CACHED_KEY,$value, self::TOKEN_EXPIRE_IN);
    }
}