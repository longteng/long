<?php
namespace app\api\service;
use app\lib\enum\ScopeEnum;
use app\lib\exception\ForbiddenException;
use app\lib\exception\ParameterException;
use app\lib\exception\TokenException;
use think\Exception;
use think\facade\Cache;
use think\facade\Request;

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/8
 * Time: 10:27
 */

class Token
{
    /**
     * @return string
     */
    public static function generateToken(){
        $randChar = charRand(8);
        $timestamp = $_SERVER['REQUEST_TIME_FLOAT'];
        $tokenSalt = config('app.wx.token_salt');
        return md5($randChar.$timestamp.$tokenSalt);
    }

    /**
     * 验证用户及以上权限
     * @return bool
     * @throws ForbiddenException
     * @throws TokenException
     */
    public static function needPrimaryScope(){
        $scope = self::getCurrentTokenVar('scope');
        if ($scope){
            if ($scope >= ScopeEnum::USER){
                return true;
            }else{
                throw new ForbiddenException();
            }
        }else{
            throw new TokenException();
        }
    }

    /**
     * 验证super及以上权限
     * @return bool
     * @throws ForbiddenException
     * @throws TokenException
     */
    public static function needSuperScope(){
        $scope = self::getCurrentTokenVar('scope');
        if ($scope){
            if ($scope >= ScopeEnum::SUPER){
                return true;
            }else{
                throw new ForbiddenException();
            }
        }else{
            throw new TokenException();
        }

    }

    /**
     * 验证用户专有权限
     * @return bool
     * @throws ForbiddenException
     * @throws TokenException
     */
    public static function needExclusivePrimaryScope(){
        $scope = self::getCurrentTokenVar('scope');
        if ($scope){
            if ($scope == ScopeEnum::USER) {
                return true;
            } else {
                throw new ForbiddenException();
            }
        } else {
            throw new TokenException();
        }
    }

    /**
     * 根据缓存获取对应的值
     * @param $key
     * @return mixed
     * @throws Exception
     * @throws TokenException
     */
    public static function getCurrentTokenVar($key){
       $token = Request::header('token');
       $vars = Cache::get($token);
       if (!$vars){
           throw new TokenException();
       }else{
           if (!is_array($vars)){
               $vars = json_decode($vars,true);
           }
           if (array_key_exists($key, $vars)){
               return $vars[$key];
           }else{
               throw new Exception('尝试获取的Token变量并不存在');
           }
       }
    }

    /**
     * 查找用户id
     * 验证super管理用户id
     * @return mixed
     * @throws ParameterException
     */
    public static function getCurrentUid(){
        $uid = self::getCurrentTokenVar('uid');
        $scope = self::getCurrentTokenVar('scope');
        if ($scope == ScopeEnum::SUPER){
            $userId = input('get.uid');
            if (!$userId){
                throw new ParameterException([
                    'msg' => '未指定操作用户对象'
                ]);
            }
            return $userId;
        }else{
            return $uid;
        }
    }

    /**
     * 验证上传用户id与缓存id是否相同
     * @param $uid
     * @return bool
     * @throws Exception
     */
    public static function isValidOperate($uid){
        if(!$uid){
            throw new Exception('检查UID时必须传入一个被检查的UID');
        }
        $user_id = self::getCurrentUid();
        if ($user_id != $uid){
            return false;
        }
        return true;
    }

    public static function verifyToken($token)
    {
        $exist = Cache::get($token);
        if($exist){
            return true;
        }
        else{
            return false;
        }
    }
}