<?php
namespace app\api\service;

use app\api\model\UserAddress;
use app\api\model\Goods;
use app\api\model\User;
use app\lib\enum\OrderStatusEnum;
use app\lib\exception\OrderException;
use app\lib\exception\UserException;
use think\Db;
use think\Exception;
use app\api\model\Order as OrderModel;
use think\facade\Log;
use app\api\model\OrderProduct;

/**
 * 订单类
 * 订单做了以下简化：
 * 创建订单时会检测库存量，但并不会预扣除库存量，因为这需要队列支持
 * 未支付的订单再次支付时可能会出现库存不足的情况
 * 所以，项目采用3次检测
 * 1. 创建订单时检测库存
 * 2. 支付前检测库存
 * 3. 支付成功后检测库存
 */
class Order
{
    /**
     * 接收前端传入商品
     * @var
     */
    protected $oProducts;
    /**
     * 数据库商品
     * @var
     */
    protected $products;
    /**
     * 用户id
     * @var
     */
    protected $uid;

    /**
     * @param int $uid 用户id
     * @param array $oProducts 订单商品列表
     * @return array 订单商品状态
     * @throws Exception
     */
    public function place($uid, $oProducts){
       $this->uid = $uid;
       $this->oProducts = $oProducts;
       $this->products = $this->getProductsByOrder($oProducts);
       $status = $this->getOrderStatus();
       if (!$status['pass']){
           $status['id'] = -1;
           return $status;
       }
       $orderSnap = $this->snapOrder();
       $status = $this->createOrderByTrans($orderSnap);
       $status['pass'] = true;
       return $status;
    }

    public function checkOrderStock($orderId){
        $orderProduct = new OrderProduct();
        $this->oProducts = $orderProduct->where('order_id', $orderId)->select();
        $this->products = $this->getProductsByOrder($this->oProducts);
        $status = $this->getOrderStatus();
        return $status;
    }

    /**
     * 根据订单商品查找数据库真实商品
     * @param $oProducts
     */
    private function getProductsByOrder($oProducts){
        $oPIds = [];
        foreach ($oProducts as $oProduct) {
            array_push($oPIds, $oProduct['product_id']);
        }

        $products = Goods::field('id,price,stock,name, main_img_url')->all($oPIds)->toArray();
        return $products;
    }

    /**
     * 查库存
     */
     private function getOrderStatus(){
        $status = [
            'pass' => true,
            'orderPrice' => 0,
            'pStatusArray' => []
        ];
        foreach ($this->oProducts as $oProduct){
            $pStatus = $this->getProductStatus($oProduct['product_id'], $oProduct['count'], $this->products);
            if (!$pStatus['hasStock']){
                $status['pass'] = false;
            }
            $status['orderPrice'] += $pStatus['totalPrice'];
            array_push($status['pStatusArray'], $pStatus);
        }
        return $status;
     }

    /**
     * 检测库存
     * @param $oPId
     * @param $oCount
     * @param $products
     * @return array
     * @throws OrderException
     */
     private function getProductStatus($oPId, $oCount, $products){
         $pIndex = -1;
         $pStatus = [
             'id' => null,
             'hasStock' => false,
             'count' => 0,
             'name' => '',
             'totalPrice' => 0
         ];

         for ($i = 0; $i < count($products); $i++){
             if ($oPId == $products[$i]['id']){
                 $pIndex = $i;
             }
         }

         if ($pIndex == -1){
             throw new OrderException([
                 'msg' => 'id为'.$oPId.'的商品不存在，订单创建失败',
             ]);
         }else{
             $product = $products[$pIndex];
             $pStatus['id'] = $product['id'];
             $pStatus['name'] = $product['name'];
             $pStatus['count'] = $oCount;
             $pStatus['totalPrice'] = $product['price'] * $oCount;
             if ($product['stock'] - $oCount >= 0){
                 $pStatus['hasStock'] = true;
             }
         }
         return $pStatus;
     }

     private function snapOrder(){
         $snap = [
             'orderPrice' => 0,
            'totalCount' => 0,
             'pStatus'  => [],
             // 根据传入地址id查找保存地址
             'snapAddress' => json_encode($this->getUserAddress()),
             'snapName' => $this->products[0]['name'],
             'snapImg'  => $this->products[0]['main_img_url']
         ];
         if (count($this->products) >1){
             $snap['snapName'] .= '等';
         }

         for ($i=0; $i<count($this->products); $i++) {
             $product = $this->products[$i];
             $oProduct = $this->oProducts[$i];

             $pStatus = $this->snapProduct($product, $oProduct['count']);
             $snap['orderPrice'] += $pStatus['totalPrice'];
             $snap['totalCount'] += $pStatus['counts'];
             array_push($snap['pStatus'], $pStatus);
         }
         return $snap;
     }

    /**
     * @return array
     * @throws UserException
     */
    private function getUserAddress()
    {
        $userAddress = UserAddress::where('user_id', '=', $this->uid)
            ->find();
        if (!$userAddress) {
            throw new UserException(
                [
                    'msg' => '用户收货地址不存在，下单失败',
                    'errorCode' => 60001,
                ]);
        }
        return $userAddress->toArray();
    }
     public function snapProduct($product, $oCount){
         $pStatus = [
             'id' => null,
             'name' => null,
             'main_img_url'=>null,
             'counts' => $oCount,
             'totalPrice' => 0,
             'price' => 0
         ];
         // 以服务器价格为准，生成订单
         $pStatus['totalPrice'] = $oCount * $product['price'];
         $pStatus['name'] = $product['name'];
         $pStatus['id'] = $product['id'];
         $pStatus['main_img_url'] =$product['main_img_url'];
         $pStatus['price'] = $product['price'];
         return $pStatus;
     }

    // 创建订单时没有预扣除库存量，简化处理
    // 如果预扣除了库存量需要队列支持，且需要使用锁机制
      public function createOrderByTrans($snap){
          // 启动事务
          Db::startTrans();
          try{
            $orderNo = $this->makeOrderNo();
            $order = new OrderModel();
            $order->user_id = $this->uid;
            $order->order_no = $orderNo;
            $order->total_price = $snap['orderPrice'];
            $order->total_count = $snap['totalCount'];
            $order->snap_img = $snap['snapImg'];
            $order->snap_name = $snap['snapName'];
            $order->snap_address = $snap['snapAddress'];
            $order->snap_items = json_encode($snap['pStatus']);
            $order->save();

            $orderID = $order->id;
            $create_time = $order->create_time;

            foreach ($this->oProducts as &$p){
                $p['order_id'] = $orderID;
            }
            $orderProduct = new OrderProduct();
            $orderProduct->saveAll($this->oProducts);
              // 提交事务
            Db::commit();
            return [
                'order_no' => $orderNo,
                'order_id' => $orderID,
                'create_time' => $create_time
            ];

        }catch(Exception $e){
              Db::rollback();
              Log::error($e);
              throw $e;
        }
      }

    /**
     * 生成订单编号
     * @return string
     */
    public function makeOrderNo(){

        $yCode = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J');
        $orderSn =
            $yCode[intval(date('Y')) - 2018] . strtoupper(dechex(date('m'))) . date(
                'd') . substr(time(), -5) . substr(microtime(), 2, 5) . sprintf(
                '%02d', rand(0, 99));
        return $orderSn;
    }

    /**
     * @param $orderID
     * @param string $jumpPage
     * @return bool
     * @throws OrderException
     */
    public function delivery($orderID, $jumpPage = ''){
        $order = OrderModel::where('id', '=', $orderID)
            ->find();
        if (!$order) {
            throw new OrderException();
        }
        if ($order->status != OrderStatusEnum::PAID) {
            throw new OrderException([
                'msg' => '还没付款呢，想干嘛？或者你已经更新过订单了，不要再刷了',
                'errorCode' => 80002,
                'code' => 403
            ]);
        }
        $order->status = OrderStatusEnum::DELIVERED;
        $order->save([
            'status'  => OrderStatusEnum::DELIVERED,
        ],['id' => $orderID]);
        $message = new DeliveryMessage();
        return $message->sendDeliveryMessage($order, $jumpPage);
    }
}