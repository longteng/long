<?php

namespace app\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

class JobTest extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('jobTest')
            ->addArgument('name', Argument::OPTIONAL, "your name")
            ->addOption('city', null, Option::VALUE_REQUIRED, 'city name')
            ->setDescription('job test');

        // 设置参数
        
    }

    protected function execute(Input $input, Output $output)
    {
        $name = trim($input->getArgument('name'));

        if ($input->hasOption('city')) {
            $city = PHP_EOL . 'From ' . $input->getOption('city');
        } else {
            $city = '';
        }

        $output->writeln("Hello," . $name . '!' . $city);
    }
}
