<?php
namespace app\lib\enum;

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/8
 * Time: 13:28
 */

class ScopeEnum
{
    const USER = 16;

    // 管理员是给CMS准备的权限
    const SUPER = 32;
}