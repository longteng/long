<?php
namespace app\lib\exception;
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/7
 * Time: 15:09
 */

class TokenException extends BaseException
{
    public $code = 401;
    public $msg = 'Token失效或过期';
    public $errorCode = 1002;
}