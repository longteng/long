<?php
namespace app\lib\exception;
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/7
 * Time: 15:09
 */

class ParameterException extends BaseException
{
    public $code = 404;
    public $msg = '找不到数据';
    public $errorCode = 2000;
}