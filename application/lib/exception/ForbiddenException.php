<?php
namespace app\lib\exception;
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/7
 * Time: 15:09
 */

class ForbiddenException extends BaseException
{
    public $code = 402;
    public $msg = '无操作权限';
    public $errorCode = 1003;
}