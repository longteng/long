<?php
namespace app\lib\exception;
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/7
 * Time: 15:09
 */

class OrderException extends BaseException
{
    public $code = 401;
    public $msg = '订单数据有误';
    public $errorCode = 1004;
}