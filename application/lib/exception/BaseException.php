<?php
namespace app\lib\exception;
use think\Exception;

/**
 * BaseException必须继承Exception，抛出错误才不会报错
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/7
 * Time: 15:09
 */

class BaseException extends Exception
{
    public $code = 400;
    public $msg = '出错了';
    public $errorCode = 999;

    public function __construct($params = [])
    {
        if(!is_array($params)){
            throw new Exception('参数必须为数组');
        }
        if(array_key_exists('code',$params)){
            $this->code = $params['code'];
        }
        if(array_key_exists('msg',$params)){
            $this->msg = $params['msg'];
        }
        if(array_key_exists('errorCode',$params)){
            $this->errorCode = $params['errorCode'];
        }
    }
}