<?php
namespace app\lib\exception;
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/7
 * Time: 15:09
 */

class WeChatException extends BaseException
{
    public $code = 400;
    public $msg = '微信请求code错误';
    public $errorCode = 1000;
}