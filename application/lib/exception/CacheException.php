<?php
namespace app\lib\exception;
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/7
 * Time: 15:09
 */

class CacheException extends BaseException
{
    public $code = 400;
    public $msg = '服务器缓存失败';
    public $errorCode = 1001;
}