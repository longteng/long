<?php
namespace app\index\controller;

use think\Controller;
use think\facade\Request;

class Index extends Controller
{
    public function index()
    {
        $a = Request::url(true);

//        var_dump($_SERVER);
//die;
        $this->redirect($a.'/cms/pages');
    }

    public function hello($name = 'ThinkPHP5')
    {
        return __DIR__;
    }
}
