<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

Route::rule('api/:version/wx/token/get','api/:version.Token/getToken','POST');
Route::rule('api/:version/wx/token/verify','api/:version.Token/verifyToken','POST');
Route::rule('api/:version/token/app','api/:version.Token/getAppToken','POST');

Route::group('api/:version', function () {
    Route::post('address/change', 'createOrUpdateAddress');
    Route::get('address/info', 'getUserAddress');
})->prefix('api/:version.UserAddress/')->pattern(['id' => '\d+']);

Route::group('api/:version', function () {
    Route::rule('goods/list', 'goodsList');
    Route::post('goods/category', 'goodsCategory');
    Route::get('goods/info/:id', 'goodsInfo');
    Route::rule('goods/recent', 'getRecent');
    Route::post('goods/add', 'goodsAdd');
})->prefix('api/:version.Goods/')->pattern(['id' => '\d+']);

Route::group('api/:version', function () {
    Route::rule('category/list', 'categoryList');
})->prefix('api/:version.Category/')->pattern(['id' => '\d+']);

Route::group('api/:version', function () {
    Route::rule('theme/list', 'themeList');
    Route::get('theme/info/:id', 'themeInfo');
    Route::rule('theme/product/:id', 'themeProduct');
    Route::post('theme/add', 'themeAdd');
})->prefix('api/:version.Theme/')->pattern(['id' => '\d+']);

Route::group('api/:version', function () {
    Route::rule('banner/list', 'bannerList');
    Route::get('banner/info/:id', 'bannerInfo');
    Route::post('banner/add', 'bannerAdd');
})->prefix('api/:version.Banner/')->pattern(['id' => '\d+']);

Route::group('api/:version', function () {
    Route::rule('user/info', 'userInfo');
    Route::post('user/login', 'userLogin');
})->prefix('api/:version.User/')->pattern(['id' => '\d+']);

Route::group('api/:version', function () {
    Route::post('order/place', 'orderPlace');
    Route::get('order/info/:id', 'orderInfo');
    Route::rule('order/user', 'orderListUser');
    Route::rule('order/paginate', 'getSummary');
    Route::put('order/delivery', 'delivery');
})->prefix('api/:version.Order/')->pattern(['id' => '\d+']);

Route::group('api/:version', function () {
    Route::post('pay/order', 'payOrder');
    Route::post('pay/notify', 'payNotify');
})->prefix('api/:version.Pay/')->pattern(['id' => '\d+']);

//微信公众号相关
Route::group('api/:version', function () {
    Route::rule('wx/access_token', 'getAccessToken');
//    Route::rule('wx/test', 'wxTest');
    Route::rule('wx/createMenu', 'createMenu');
    Route::rule('wx/test', 'getOpenid');
})->prefix('api/:version.Token/')->pattern(['id' => '\d+']);