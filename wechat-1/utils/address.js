import { Base } from "./base.js"
var base = new Base();

class Address extends Base{
  constructor() {
    super();
  }

  setAddressInfo(res){
    var province = res.provinceName || res.province,
      city = res.cityName || res.city,
      country = res.countyName || res.country,
      detail = res.detailInfo || res.detail;
    var totalDetail = city + country + detail;

    //直辖市，取出省部分
    if (!this.isCenterCity(province)) {
      totalDetail = province + totalDetail;
    };
    return totalDetail;
  }

  /*是否为直辖市*/
  isCenterCity(name) {
    var centerCitys = ['北京市', '天津市', '上海市', '重庆市'],
      flag = centerCitys.indexOf(name) >= 0;
    return flag;
  }

  submitAddress(data, callBack){
    data = this._setUpAddress(data);
    var param = {
      url: 'api/v1/address/change',
      method: 'POST',
      data: data,
      sCallBack: function (res) {
        callBack && callBack(true, res);
      },
      eCallBack: function (res) {
        callBack && callBack(false, res);
      },
    };
    this.request(param);
  }

  _setUpAddress (res) {
    var addressInfo = {
      name: res.userName,
      province: res.provinceName,
      city: res.cityName,
      country: res.countyName,
      mobile: res.telNumber,
      detail: res.detailInfo
    };
    return addressInfo;
  }

  getAddress(callBack) {
    var that = this;
    var param = {
      url: 'api/v1/address/info',
      sCallBack: function (res) {
        res.totalDetail = that.setAddressInfo(res);
        callBack && callBack(res);
      },
      eCallBack: function (res) {
        callBack && callBack(res);
      },
    };
    this.request(param);
  }
}
export {Address}