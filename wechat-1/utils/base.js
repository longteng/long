import { Config } from "./config.js";
import { Token } from './token.js';
var tokrn = new Token();

class Base {
  constructor() {
    this.baseRequestUrl = Config.restUrl;
  }

  request(params, noRefetch) {
    var that = this;
    if (!params.method){
      params.method = 'GET';
    } 
    if (!params.dataType) {
      params.dataType = "json";
    } 
    if (!params.responseType) {
      params.responseType = "text";
    } 
    var url = this.baseRequestUrl+params.url;
    /*不需要再次组装地址*/
    if (params.setUpUrl == false) {
      url = params.url;
    }
    wx.request({
      url: url,
      data: params.data,
      header: {
        'content-type':'application/json',
        'token':wx.getStorageSync('token')
      },
      method: params.method,
      dataType: params.type,
      responseType: params.responseType,
      success: function (res) { 
        // if(params.sCallBack){
        //   params.sCallBack(res);
        // }
        var code = res.statusCode.toString();
        var startChar = code.charAt(0);
        if(startChar == '2'){
          params.sCallBack && params.sCallBack(res.data);
        }else{
          if(code=='401'){
            if (!noRefetch){
              that._refetch(params);
            }
          }
          if (noRefetch) {
            params.eCallBack && params.eCallBack(res.data);
          }
          that._processError(res);
        }
      },
      fail: function (error) {
        that._processError(err);
       },
      complete: function (res) { },
    })
  }

  _processError(err) {
    console.log(err);
  }

  _refetch(param) {
    var token = new Token();
    token.getTokenFromServer((token) => {
      this.request(param, true);
    });
  }

  getDataSet(event, key){
    return event.currentTarget.dataset[key];
  }
}
export { Base }