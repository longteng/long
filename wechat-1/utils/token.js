// 引用使用es6的module引入和定义
// 全局变量以g_开头
// 私有函数以_开头
import { Config } from 'config.js';

class Token {
    constructor() {
      this.tokenUrl = Config.restUrl + 'api/v1/wx/token/get';
      this.verifyUrl = Config.restUrl + 'api/v1/wx/token/verify';
    }

    verify() {
        var token = wx.getStorageSync('token');
        if (!token) {
            this.getTokenFromServer();
        }
        else {
            this._veirfyFromServer(token);
        } 
    }

    _veirfyFromServer(token) {
        var that = this;
        wx.request({
            url: that.verifyUrl,
            method: 'POST',
            data: {
                token: token
            },
            success: function (res) {
                var valid = res.data.isValid;
                if(!valid){
                    that.getTokenFromServer();
                }
            }
        })
    }

    getTokenFromServer(callBack) {
      var that  = this;
      wx.login({
        success(res) {
          var code = res.code;
          if (code) {
            //发起网络请求
            wx.request({
              url: that.tokenUrl,
              data: {
                code: code
              },
              method: 'POST',
              success(res) {
                wx.setStorageSync('token', res.data.token);
                callBack && callBack(res.data.token);
              },
              fail(res) {
                console.log(res.data);
              }
            })
          } else {
            console.log('登录失败！' + res.errMsg)
          }
        }
      })
    }
}

export {Token};