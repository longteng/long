import { Base } from "../../utils/base.js"
class My extends Base {
  constructor() {
    super();
  }

  getUserInfo(cb) {
    var that = this;
      // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              typeof cb == "function" && cb(res.userInfo);

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (that.userInfoReadyCallback) {
                that.userInfoReadyCallback(res)
              }
            }
          })
        }else{
          typeof cb == "function" && cb(null);
        }
      }
    })
  }

}
export { My }