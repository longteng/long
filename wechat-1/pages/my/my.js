import { My } from "./my-model.js"
import {Address} from "../../utils/address.js"
import {Order} from "../order/order-model.js"

var my = new My();
var address = new Address();
var order = new Order();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: null,
    pageIndex: 1,
    isLoadedAll: false,
    orderArr:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this._loadData();
    this._getAddressInfo();
  },
  onShow: function () {
    //更新订单,相当自动下拉刷新,只有  非第一次打开 “我的”页面，且有新的订单时 才调用。
    var newOrderFlag = order.hasNewOrder();
    if (newOrderFlag) {
      this.onPullDownRefresh();
    }
  },

  _loadData: function(){
    my.getUserInfo((data)=>{
      this.setData({
        userInfo: data
      })
    });
    this._getOrders();
    order.execSetStorageSync(false);  //更新标志位
  },

  getUserInfo: function () {
    this._loadData();
  },

  _getAddressInfo: function (){
    address.getAddress((res)=>{
      this._bindAddressInfo(res);
    })
  },

  /*绑定地址信息*/
  _bindAddressInfo: function (addressInfo) {
    this.setData({
      addressInfo: addressInfo
    });
  },
  _getOrders: function (callback){
    var that = this;
    order.getOrders(this.data.pageIndex,(res)=>{
      var data = res.data;
      that.setData({
        loadingHidden: true
      });
      if (data.length > 0) {
        that.data.orderArr.push.apply(that.data.orderArr, res.data);  //数组合并
        that.setData({
          orderArr: that.data.orderArr
        });
      } else {
        that.data.isLoadedAll = true;  //已经全部加载完毕
        that.data.pageIndex = 1;
      }
      callback && callback();
    })
  },
  /*下拉刷新页面*/
    onPullDownRefresh: function () {
    var that = this;
    this.data.orderArr = [];  //订单初始化
    this._getOrders(() => {
      that.data.isLoadedAll = false;  //是否加载完全
      that.data.pageIndex = 1;
      wx.stopPullDownRefresh();
      order.execSetStorageSync(false);  //更新标志位
    });
  },
  /**
   * 上拉触发函数
   */
  onReachBottom: function () {
    if (!this.data.isLoadedAll) {
      this.data.pageIndex++;
      this._getOrders();
    }
  },
  showOrderDetailInfo(event) {
    var id = order.getDataSet(event,'id');
    wx.navigateTo({
      url: "../order/order?from=order&id="+id,
    })
  },
  /*未支付订单再次支付*/
  rePay: function (event) {
    var id = order.getDataSet(event, 'id'),
      index = order.getDataSet(event, 'index');

    //online 上线实例，屏蔽支付功能
    if (order.onPay) {
      this._execPay(id, index);
    } else {
      this.showTips('支付提示', '本产品仅用于演示，支付系统已屏蔽');
    }
  },
  /*支付*/
  _execPay: function (id, index) {
    var that = this;
    order.execPay(id, (statusCode) => {
      if (statusCode > 0) {
        var flag = statusCode == 0;

        //更新订单显示状态
        if (flag) {
          that.data.orderArr[index].status = 2;
          that.setData({
            orderArr: that.data.orderArr
          });
        }

        //跳转到 成功页面
        wx.navigateTo({
          url: '../pay-result/pay-result?id=' + id + '&flag=' + flag + '&from=my'
        });
      } else {
        that.showTips('支付失败', '商品已下架或库存不足');
      }
    });
  },
  editAddress: function (event) {
    var that = this;
    wx.chooseAddress({
      success: function (res) {
        var addressInfo = {
          name: res.userName,
          mobile: res.telNumber,
          totalDetail: address.setAddressInfo(res),
        }
        that._bindAddressInfo(addressInfo);
        address.submitAddress(res, (flag) => {
          if (!flag) {
            that.showTips('操作提示', '操作提示地址信息更新失败')
          }
        });
      }
    })
  },
  /*
    * 提示窗口
    * params:
    * title - {string}标题
    * content - {string}内容
    * flag - {bool}是否跳转到 "我的页面"
    */
  showTips: function (title, content) {
    wx.showModal({
      title: title,
      content: content,
      showCancel: false,
      success: function (res) {
      }
    });
  },

  testSubmit: function (e) {
    console.log(e.detail.formId);
    var self = this;
    var _access_token = '16_TE1IA7vVeuVbysKS9jS-3YVkzlsVziuhVJLYxsMNXv4Sxh1JnKHC98a-8v-rrxWy1z0Eyj31NPf4dbJ0wg1d8R8I2GWRPB4o545t010xM8spj8wawb58fyWHCZojPtOVtYgkuRGb3020eVzWXFQaAGASAL';
    var url = 'https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send?access_token=' + _access_token

      ; var _jsonData = {
        access_token: _access_token,
        touser: 'oOYX64pa9twrLtVRp0nhhBan_9ww',
        template_id: 'UyOzKRI62p6AANs-E1yc7KJLiUOVBm4dL5CHqKHdg2A',
        form_id: e.detail.formId,
        page: "../pages/home/home",
        data: {
          "keyword1": { "value": "测试数据一", "color": "#173177" },
          "keyword2": { "value": "测试数据二", "color": "#173177" },
          "keyword3": { "value": "测试数据三", "color": "#173177" },
          "keyword4": { "value": "测试数据四", "color": "#173177" },
          "keyword5": { "value": "测试数据四", "color": "#173177" },
          "keyword6": { "value": "测试数据四", "color": "#173177" },
        }
      }
    // wx.request({
    //   url: url,
    //   data: _jsonData,
    //   method: 'POST',
    //   success: function (res) {
    //     console.log(res)
    //   },
    //   fail: function (err) {
    //     console.log('request fail ', err);
    //   },
    //   complete: function (res) {
    //     console.log("request completed!");
    //   }
    // })
  }
})