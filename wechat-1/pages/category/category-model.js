import { Base } from "../../utils/base.js"
class Category extends Base {
  constructor() {
    super();
  }
  getCategoryType(callBack) {
    var params = {
      url: 'api/v1/category/list',
      // method: 'GET',
      sCallBack: function (res) {
        callBack && callBack(res);
      }
    }
    this.request(params);
  }
  getProductsCategory(id,callBack) {
    var params = {
      url: 'api/v1/goods/category?id='+id,
      method: 'POST',
      sCallBack: function (res) {
        callBack && callBack(res);
      }
    }
    this.request(params);
  }
}
export { Category }