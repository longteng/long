import { Category } from "./category-model.js"
var category = new Category();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    categoryAll: {},
    currentMenuIndex:0,
    loadData: {},
    categoryProduct: {},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.data.id = options.id;
    this._loadData();
  },

  onReady: function () {
    wx.setNavigationBarTitle({
      title: '商品详情',
    })
  },

  _loadData: function () {
    category.getCategoryType((categoryData) => {
      this.setData({
        categoryAll: categoryData,
      });
      category.getProductsCategory(categoryData[0].id, (data) => {
        var dataObj = {
          product: data,
          topImgUrl: categoryData[0].image.url,
          title: categoryData[0].name
        }
        this.setData({
          categoryProduct: dataObj,
        });
        this.data.loadData[0] = dataObj;
      });
    });
  },

  changeCategory: function (event) {
    var index = category.getDataSet(event,'index');
    this.setData({
      currentMenuIndex: index,
    });
    var id = category.getDataSet(event, 'id');
    if (!this.isLoadData(index)) {
      category.getProductsCategory(id, (data) => {
        var dataObj = {
          product: data,
          topImgUrl: this.data.categoryAll[index].image.url,
          title: this.data.categoryAll[index].name
        }
        this.setData({
          categoryProduct: dataObj,
        });
        this.data.loadData[index] = dataObj;
      });
    }else{
      this.setData({
        categoryProduct: this.data.loadData[index],
      });
    }
  },
  onProductsItemTap: function (event) {
    var id = category.getDataSet(event, 'id');
    wx.navigateTo({
      url: '../product/product?id=' + id,
    })
  },
  isLoadData: function (index) {
    if(this.data.loadData[index]){
      return true;
    }
    return false;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})