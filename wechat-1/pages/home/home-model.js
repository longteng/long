import {Base} from "../../utils/base.js"
class Home extends Base{
  constructor() {
    super();
  }
  getBannerData(callBack) {
    var params = {
      url: 'api/v1/banner/list',
      // method: 'GET',
      sCallBack: function(res){
        callBack && callBack(res);
      }
    }
    this.request(params);
  }
  getThemeData(callBack) {
    var params = {
      url: 'api/v1/theme/list',
      sCallBack: function (res) {
        callBack && callBack(res);
      }
    }
    this.request(params);
  }
  getProductRecentData(callBack) {
    var params = {
      url: 'api/v1/goods/recent',
      sCallBack: function (res) {
        callBack && callBack(res);
      }
    }
    this.request(params);
  }
}
export { Home }