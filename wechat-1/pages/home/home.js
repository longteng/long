// pages/index/home.js
import { Home } from './home-model.js';
var home = new Home();
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this._loadData();
  },
  _loadData: function () {
    home.getBannerData((res)=>{
      // console.log(res.banner_item);
      this.setData({
        "bannerList": res.banner_item,
      })
    });
    home.getThemeData((res) => {
      // console.log(res);
      this.setData({
        "themeArr": res,
      })
    });
    home.getProductRecentData((res) => {
      // console.log(res);
      this.setData({
        productsArr: res,
      })
    });
  },

  onProductsItemTap: function (event){
    var id = home.getDataSet(event, 'id');
    wx.navigateTo({
      url: '../product/product?id='+id,
    })
  },

  onThemeItemTap: function (event) {
    var id = home.getDataSet(event, 'id');
    var name = home.getDataSet(event, 'name');
    wx.navigateTo({
      url: '../theme/theme?id=' + id +'&name='+name,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})