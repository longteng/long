import {Theme} from "./theme-model.js"
var theme = new Theme();
// pages/theme/theme.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.data.id = options.id;
    this.data.name = options.name;
    this._loadData(options);
  },

  onReady: function () {
    wx.setNavigationBarTitle({
      title: this.data.name,
    })
  },

  _loadData: function () {
    theme.getProductData(this.data.id, (res) => {
      this.setData({
        productInfo: res
      });
    });
  },
  onProductsItemTap: function (event) {
    var id = theme.getDataSet(event, 'id');
    wx.navigateTo({
      url: '../product/product?id=' + id,
    })
  },

})