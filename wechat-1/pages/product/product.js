import {Product} from "./product-model.js"
import {Cart} from "../cart/cart-model.js"
var product = new Product();
var cart = new Cart();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    array: [1,2,3,4,5,6,7],
    productCount: 1,
    cardTitle: ['商品详情','产品参数','售后保障'],
    currentTabsIndex: 0,
    product: {},
    cartTotalCount: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.data.id = options.id;
    this._loadData();
  },

  onReady: function () {
    wx.setNavigationBarTitle({
      title: '商品详情',
    })
  },

  _loadData: function () {
    product.getDetailInfo(this.data.id, (res) => {
      this.setData({
        product: res,
        productCount: res.stock==0?0:1
      });
    });
    this.setData({
      cartTotalCount: cart.getCartConutFromLocal(),
    })
  },
  /**
   * 选择商品数量
   */
  bindPickerChange: function (event) {
    var selectCount = this.data.array[event.detail.value];
    this.setData({
      productCount: selectCount
    })
  },
  /**
   * 产品参数、详情、售后选中
   */
  onTabsItemTap: function (event) {
    var index = product.getDataSet(event, 'index');
    this.setData({
      currentTabsIndex: index
    })
  },
  /**
   * 加入购物车
   */
  onAddingToCartTap: function (event) {
    this.addToCart();
    var counts = this.data.cartTotalCount + this.data.productCount;
    this.setData({
      cartTotalCount: counts,
    })
  },
  addToCart: function () {
    var tmpObj = {};
    var keys = ['id', 'name', 'main_img_url', 'price'];
    for (var key in this.data.product) {
      if (keys.indexOf(key) >= 0) {
        tmpObj[key] = this.data.product[key]
      }
    }
    cart.add(tmpObj, this.data.productCount);
  },
  onCartTap: function () {
    wx.switchTab({
      url: "../cart/cart",
    })
  }
})