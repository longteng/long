import { Base } from "../../utils/base.js"
class Product extends Base{
  constructor() {
    super();
  }
  getDetailInfo(id, callBack) {
    var params = {
      url: 'api/v1/goods/info/' + id,
      // method: 'GET',
      sCallBack: function (res) {
        callBack && callBack(res);
      }
    }
    this.request(params);
  }
}
export { Product }