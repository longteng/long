import { Base } from "../../utils/base.js"
class Order extends Base {
  constructor() {
    super();
    // 标识是否有新订单，个人中心订单使用
    this._storageKeyName = 'newOrder';
  }
  
  doOrder(param,callback){
    var that=this;
    var allParams = {
      url: 'api/v1/order/place',
      method: 'POST',
      data: { products: param },
      sCallBack: function (data) {
        that.execSetStorageSync(true);
        callback && callback(data);
      },
      eCallBack: function () {
      }
    };
    this.request(allParams);
  }
  /*
   * 拉起微信支付
   * params:
   * norderNumber - {int} 订单id
   * return：
   * callback - {obj} 回调方法 ，返回参数 可能值 0:商品缺货等原因导致订单不能支付;  1: 支付失败或者支付取消； 2:支付成功；
   * */
  execPay(orderNumber, callback) {
    var allParams = {
      url: 'api/v1/pay/order',
      method: 'POST',
      data: { id: orderNumber },
      sCallBack: function (data) {
        var timeStamp = data.timeStamp;
        if (timeStamp) { //可以支付
          wx.requestPayment({
            'timeStamp': timeStamp.toString(),
            'nonceStr': data.nonceStr,
            'package': data.package,
            'signType': data.signType,
            'paySign': data.paySign,
            success: function () {
              callback && callback(2);
            },
            fail: function () {
              callback && callback(1);
            }
          });
        } else {
          callback && callback(0);
        }
      }
    };
    this.request(allParams);
  }

  /*本地缓存 保存／更新*/
  execSetStorageSync(data) {
    wx.setStorageSync(this._storageKeyName, data)
  }
  /*获得订单的具体内容*/
  getOrderInfoById(id, callback) {
    var that = this;
    var allParams = {
      url: 'api/v1/order/info/' + id,
      sCallBack: function (data) {
        callback && callback(data);
      },
      eCallBack: function () {

      }
    };
    this.request(allParams);
  }
  /*获得所有订单,pageIndex 从1开始*/
  getOrders(pageIndex, callback) {
    var allParams = {
      url: 'api/v1/order/user',
      data: { page: pageIndex},
      method: 'GET',
      sCallBack: function (data) {
        callback && callback(data);  //1 未支付  2，已支付  3，已发货，4已支付，但库存不足
      }
    };
    this.request(allParams);
  }

  /*是否有新的订单*/
  hasNewOrder() {
    var flag = wx.getStorageSync(this._storageKeyName);
    return flag == true;
  }

}
export { Order }