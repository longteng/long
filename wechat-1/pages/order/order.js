import { Cart } from "../cart/cart-model.js"
import { Address } from "../../utils/address.js"
import { Order } from "./order-model.js"
var cart = new Cart();
var address = new Address();
var order = new Order();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    addressInfo: null,
    id: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var from = options.from;
    if(from=='cart'){
      this._fromCart(options.account);
    }else{
      this._fromOrder(options.id);
    }
  },
  /**
  * 生命周期函数--监听页面显示
  */
  onShow: function () {
    if (this.data.id) {
      this._fromOrder(this.data.id);
    }
  },

  _fromCart(account){
    this.data.account = account;
    this.setData({
      productsArr: cart.getCartDataFromLocal(true),
      account: account,
      orderStatus: 0
    });

    /*显示收获地址*/
    address.getAddress((res) => {
      this._bindAddressInfo(res);
    });
  },
  _fromOrder(id){
    var that = this;
    //下单后，支付成功或者失败后，点左上角返回时能够更新订单状态 所以放在onshow中
    order.getOrderInfoById(id, (data) => {
      that.setData({
        orderStatus: data.status,
        productsArr: data.snap_items,
        account: data.total_price,
        basicInfo: {
          orderTime: data.create_time,
          orderNo: data.order_no
        },
      });

      // 快照地址
      var addressInfo = data.snap_address;
      addressInfo.totalDetail = address.setAddressInfo(addressInfo);
      that._bindAddressInfo(addressInfo);
    });
  },

  editAddress: function (event) {
    var that = this;
    wx.chooseAddress({
      success: function(res){
        var addressInfo = {
          name: res.userName,
          mobile: res.telNumber,
          totalDetail: address.setAddressInfo(res),
        }
        that._bindAddressInfo(addressInfo);
        address.submitAddress(res,(flag) => {
          if(!flag){
            that.showTips('操作提示','操作提示地址信息更新失败')
          }
        });
      }
    })
  },

  showTips: function (title, content, flag){
    wx.showModal({
      title: title,
      content: content,
      showCancel: false,
      success: function (res) {
        if(flag){
          wx.switchTab({
            url: '../my/my',
          })
        }
      }
    })
  },

  _bindAddressInfo: function (addressInfo) {
    this.setData({
      addressInfo: addressInfo,
    });
  },

  pay: function () {
    if(!this.data.addressInfo){
      this.showTips('下单提示', '请填写您的收货地址');
      return;
    }
    if(this.data.orderStatus==0){
      this._firstTimePay();
    }else{
      this._oneMoresTimePay();
    }
  },

//第一次支付
  _firstTimePay: function(){
    var orderInfo = [];
    var productInfo = this.data.productsArr;
    for(let i=0; i<productInfo.length; i++){
      orderInfo.push({
        product_id:productInfo[i].id,
        count:productInfo[i].counts,
      });
    }
    var that = this;
    order.doOrder(orderInfo,(data)=>{
      if(data.pass){
        var id = data.order_id;
        that.data.id=id;
        that.data.fromCartFlag= false;
        //开始支付
        that._execPay(id);
      }else{
        that._orderFail(data);
      }
    });
  },

  _orderFail: function(data){
    var nameArr = [],
      name = '',
      str = '',
      pArr = data.pStatusArray;
    for(let i=0; i<pArr.length; i++){
      if(!pArr[i].haveStock){
        name= pArr[i].name;
        if(name.length>15){
          name = name.substr(0.12)+'...';
        }
        nameArr.push(name);
        if(nameArr.length>2){
          break;
        }
      }
      str = nameArr.join("、");
      if (nameArr.length >= 2) {
        str += ' 等';
      }
      str += ' 缺货';
    }
    wx.showModal({
      title: '下单失败',
      content: str,
      showCancel: false,
      success: function (res) {

      }
    });
  },
  /* 再次次支付*/
  _oneMoresTimePay: function () {
    this._execPay(this.data.id);
  },
    /*
    *开始支付
    * params:
    * id - {int}订单id
    */
  _execPay: function (id) {
    if (!order.onPay) {
      this.showTips('支付提示', '本产品仅用于演示，支付系统已屏蔽', true);//屏蔽支付，提示
      this.deleteProducts(); //将已经下单的商品从购物车删除
      return;
    }
    var that = this;
    order.execPay(id, (statusCode) => {
      if (statusCode > 0) {
        that.deleteProducts(); //将已经下单的商品从购物车删除   当状态为0时，表示

        var flag = statusCode == 2;
        wx.navigateTo({
          url: '../pay-result/pay-result?id=' + id + '&flag=' + flag + '&from=order'
        });
      }else{
        this.showTips('支付', '调起接口失败')
      }
    });
  },
  //将已经下单的商品从购物车删除
  deleteProducts: function () {
    var ids = [], arr = this.data.productsArr;
    for (let i = 0; i < arr.length; i++) {
      ids.push(arr[i].id);
    }
    cart.deleteProductfromCart(ids);
  },

  
})