import { Base } from "../../utils/base.js"
class Cart extends Base{
  constructor(){
    super();
    this._storageKeyName = 'cart';
  }

  /**
   * 添加购物车
   */
  add(item, counts) {
    var cartData = this.getCartDataFromLocal();
    var isHasInfo = this._isHasThatOne(item.id, cartData);
    if(isHasInfo.index == -1){
      item.counts = counts;
      item.selectStatus = true;
      cartData.push(item);
    }else{
      cartData[isHasInfo.index].counts += counts;
    }
    wx.setStorageSync(this._storageKeyName, cartData)
  }

  /**
   * 本地缓存 保存、更新
   */
  execSetStorageSync(data) {
    wx.setStorageSync(this._storageKeyName, data)
  }

  /**
   * 获取缓存中购物车商品
   */
  getCartDataFromLocal(flag) {
    var res = wx.getStorageSync(this._storageKeyName);
    if(!res){
      return res = [];
    }
    if(flag){
      var newRes = [];
      for (let i=0; i<res.length; i++){
        if(res[i].selectStatus){
          newRes.push(res[i]);
        }
      }
      return newRes;
    }

    return res;
  }
  /**
   * 获取缓存中购物车商品数量
   */
  getCartConutFromLocal(flag) {
    var res = wx.getStorageSync(this._storageKeyName);
    var count = 0
    if (!res) {
      return count;
    }
    for (let i = 0; i < res.length; i++){
      if(flag){
        if (res[i].selectStatus){
          count += res[i].counts;
        }
      }else{
        count += res[i].counts;
      }
    }
    return count;
  }
  /**
   * 判断商品是否存在购物车里面
   */
  _isHasThatOne(id, data) {
    var item;
    var result = {index: -1};
    for(let i = 0; i<data.length; i++){
      item = data[i];
      if(item.id == id){
        result = {
          index: i,
          data: item,
        }
        break;
      };
    }
    return result;
  }

/**
 * 改变数量
 */
  changeCounts(id, counts){
    var cartData = this.getCartDataFromLocal();
    var hasInfo = this._isHasThatOne(id, cartData);
    if (hasInfo.index != -1 && (hasInfo.data.counts+counts)>0){
      cartData[hasInfo.index].counts += counts ;
    }
    wx.setStorageSync(this._storageKeyName, cartData);
  }
  /**
   * 增加购物车商品数量
   */
  addCounts(id) {
   this.changeCounts(id, 1);
  }
  /**
   * 减少购物车商品数量
   */
  cutCounts(id) {
    this.changeCounts(id, -1);
  }
  /**
   * 删除购物车指定商品
   */
  deleteProductfromCart(ids) {
    if(!(ids instanceof Array)){
      ids = [ids];
    }

    var cartData = this.getCartDataFromLocal();
    for(let i=0; i<ids.length; i++){
      var hasInfo = this._isHasThatOne(ids[i], cartData);
      if(hasInfo.index != -1){
        cartData.splice(hasInfo.index, 1);
      }
    }
    wx.setStorageSync(this._storageKeyName, cartData);
  }
}
export {Cart}