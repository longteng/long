import { Cart } from "./cart-model.js"
var cart = new Cart();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cartData: {},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },

  onHide: function () {
    cart.execSetStorageSync(this.data.cartData)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onShow: function (options) {
    var cartData = cart.getCartDataFromLocal();

    // var countsInfo = cart.getCartConutFromLocal(true);
    var cal = this._calcTotalAccountAndCounts(cartData);
    this.setData({
      selectedCounts: cal.selectedCounts,
      selectedTypeCounts: cal.selectedTypeCounts,
      account: cal.account,
      cartData: cartData,
    });
  },
  /**
   * 获取购物车相关信息
   */
  _calcTotalAccountAndCounts: function (data) {
    var len = data.length;
  
    //选中商品总价
    var account = 0;
    //选中商品总数量
    var selectedCounts = 0;
    //选中商品总类型数量
    var selectedTypeCounts = 0;

    let multiple = 100;

    for (let i=0; i<len; i++) {
      if(data[i].selectStatus){
        account += data[i].counts*multiple * Number(data[i].price)*multiple;
        selectedCounts += data[i].counts;
        selectedTypeCounts++;
      }
    }
    account = account/(multiple*multiple);
    return { account, selectedCounts, selectedTypeCounts};

  },

  toggleSelect: function (event) {
    var id = cart.getDataSet(event, 'id');
    var status = cart.getDataSet(event, 'status');
    var index = cart.getDataSet(event, 'index');
    this.data.cartData[index].selectStatus = !status;
    this._resetCartData();
  },
  toggleSelectAll: function (event) {
    var status = cart.getDataSet(event, 'status') == 'true';

    var data = this.data.cartData;
    for (let i = 0; i < data.length; i++) {
      data[i].selectStatus = !status;
    }
    this._resetCartData();
  },
  // 重置购物车选中数据
  _resetCartData: function (){
    var res = this._calcTotalAccountAndCounts(this.data.cartData);
    this.setData({
      selectedCounts: res.selectedCounts,
      selectedTypeCounts: res.selectedTypeCounts,
      account: res.account,
      cartData: this.data.cartData,
    });
  },
  /**
   * 根据id 获取对应index下标
   */
  _getProductIndexById: function (id){
    var data = this.data.cartData;
    for (let i = 0; i < data.length; i++) {
      if (data[i].id == id){
        return i;
      }
    }
  },

  changeCounts: function (event) {
    var id = cart.getDataSet(event, 'id');
    var type = cart.getDataSet(event, 'type');
    var index = this._getProductIndexById(id);
    var counts = 1;
    if (type=='add'){
      cart.addCounts(id);
    }else{
      counts = -1
      cart.cutCounts(id);
    }
    var cartData = cart.getCartDataFromLocal();

    // var countsInfo = cart.getCartConutFromLocal(true);
    this.data.cartData[index].counts += counts;
    this._resetCartData();
  },

  delete: function (event) {
    var id = cart.getDataSet(event, 'id');
    var index = this._getProductIndexById(id);
    this.data.cartData.splice(index,1);
    this._resetCartData();
    cart.deleteProductfromCart(id);
  },

  submitOrder: function (event) {
    wx.navigateTo({
      url: '../order/order?account='+ this.data.account+'&from=cart'
    });
  }

})