//index.js
//获取应用实例
const app = getApp()
var baseUrl = 'http://www.long.test'
Page({
  data: {
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse){
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }
  },
  getUserInfo: function(e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
  getToken: function () {
    wx.login({
      success(res) {
        var code = res.code;
        // console.log(code);
        if (code) {
          //发起网络请求
          wx.request({
            url: baseUrl + '/api/v1/wx/token',
            data: {
              code: code
            },
            method: 'POST',
            success(res) {
              console.log(res.data.token);
              wx.setStorageSync('token', res.data.token);
            },
            fail(res) {
              console.log(res.data);
            }
          })
        } else {
          console.log('登录失败！' + res.errMsg)
        }
      }
    })
  },
  pay: function () {
    var token = wx.getStorageSync('token');
    var that = this;
    wx.request({
      url: baseUrl + '/api/v1/order/place',
      header: {
        token: token
      },
      data: {
        products:
        [
          {
            product_id: 1, count: 2
          },
          {
            product_id: 2, count: 1
          }
        ]
      },
      method: 'POST',
      success(res) {
        console.log(res.data);
        if (res.data.pass) {
          wx.setStorageSync('order_id', res.data.order_id);
          that.getPreOrder(token, res.data.order_id);
        }else{
          console.log('订单未创建成功');
        }
      }
    })
  },
  getPreOrder: function (token, orderID) {
    if(token){
      wx.request({
        url: baseUrl + '/api/v1/pay/order',
        header: {
          token: token
        },
        data: {
         id: oederID
        },
        success(res) {
          var preData = res.data;
          console.log(preData);
        }
      })
    }
  }
})
